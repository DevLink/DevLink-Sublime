import sublime
import sublime_plugin
import subprocess


class DevlinkCommand(sublime_plugin.TextCommand):
	def run(self, edit):
		file = self.view.file_name()
		devlinkURL = "devlink:localFile?{file}".format(file=file)
		subprocess.call([
                '/usr/bin/open',
                devlinkURL
            ])
